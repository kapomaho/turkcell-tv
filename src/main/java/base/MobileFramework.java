package base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class MobileFramework {

	
	
	
	public AndroidDriver driver;
	public WebDriverWait wait;
	public String url;
	public Properties prop;
	public static String accessKey = "wDia5WAnxGpxspmQ9MpX";
	  public static String userName = "mahirkaplanc1";

 	private DesiredCapabilities caps  ;

 	  // For remote run
	  //public static String userName = "mahirkaplanc1";
	  //public static String accessKey = "wDia5WAnxGpxspmQ9MpX";
	  
	
	public void init(String testAdi) throws FileNotFoundException, IOException{
		
		//com.turkcell.ott
		//com.turkcell.ott.login.LoginActivity
		
		
		//Testler'in hangi ortamlarda kosulacagi
		String env=getConfDepend("platform");
		String device=getConfDepend("device");
		if(env.compareTo("local")==0)
		{
		System.out.println("ada");
		 this.caps = new DesiredCapabilities();
	//	this.caps.setCapability("deviceName", "mahos");
			this.caps.setCapability("deviceName", device);

		this.caps.setCapability("app", getConfDepend("apk"));
		caps.setCapability("noReset", "false");
		//caps.setCapability("resetKeyboard", true);
		//caps.setCapability("unicodeKeyboard", true);
		this.driver =new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),caps);
		//driver.rotate(ScreenOrientation.PORTRAIT);
	
		wait = new WebDriverWait(driver, 10);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		}
		else if (env.compareTo("noreset")==0)
		{
			
			System.out.println("noreset");
			 this.caps = new DesiredCapabilities();
				caps.setCapability("platformName", "Android");
				
				//7374d6d2
				this.caps.setCapability("deviceName", device);
				caps.setCapability("noReset", "true");
				caps.setCapability("automationName", "UiAutomator2");
				//caps.setCapability("skipUnlock", "true");
 
				 
			//this.caps.setCapability("deviceName", "mahos");
			caps.setCapability("appPackage", "com.turkcell.ott");
			caps.setCapability("appActivity", "com.turkcell.ott.login.LoginActivity");
			this.driver =new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),caps);
			//driver.rotate(ScreenOrientation.PORTRAIT);

			wait = new WebDriverWait(driver, 20);
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			
		}
		else if (env.compareTo("cloud")==0)
		{
			
			caps = new DesiredCapabilities();

			caps.setCapability("device", "iPhone X");
			caps.setCapability("os_version", "11.0");
		    caps.setCapability("noReset", "true");
			caps.setCapability("automationName", "UiAutomator2");
			caps.setCapability("skipUnlock", "true");
		    caps.setCapability("app", "bs://04e568be3aa3185659a65aa0a6925d359857d01f");
		    this.driver = new AndroidDriver(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), caps);
		}
		else
		{
			
			System.out.println("ada");
			 this.caps = new DesiredCapabilities();
				caps.setCapability("platformName", device);
				
				//7374d6d2
				this.caps.setCapability("deviceName", "7374d6d2");
				caps.setCapability("noReset", "false");

			//this.caps.setCapability("deviceName", "mahos");
			caps.setCapability("appPackage", "com.turkcell.ott");
			caps.setCapability("appActivity", "com.turkcell.ott.login.LoginActivity");
			this.driver =new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),caps);
			//driver.rotate(ScreenOrientation.PORTRAIT);
		
			wait = new WebDriverWait(driver, 20);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
		}
		
	}
	
	public String getConfDepend(String value) throws FileNotFoundException, IOException
	{
		prop = new Properties();
		prop.load(new FileInputStream("src/test/resources/config.properties"));
		String val = prop.getProperty(value);
		return val;
	}
	
	
	public void click(AndroidElement element) throws InterruptedException{
		try{

			//wait.until(ExpectedConditions.elementToBeClickable(element));
		//String id =element.getAttribute("id");
			
			element.click();
		
		}
//
		catch(StaleElementReferenceException e){
				
			click(element);
		}
		
		catch(Exception e){
			System.out.println(e.getMessage());
		}		
	}
	
	public void beklemeSuresiBelirt(long i) throws InterruptedException{
		Thread.sleep(i);	
	}
	
	
	public void write(MobileElement element,String metin) throws InterruptedException{
		try{
			wait.until(ExpectedConditions.visibilityOf(element));
		//	element.clear();
			element.setValue(metin);
			
			 
		//	element.sendKeys(Keys.TAB);
			//ATUReports.add("Xpath par�as� ile yaz�lm��t�r.", element.getAttribute("id"), metin, "PASS", false);
		}
		
		catch(StaleElementReferenceException e){
			write(element, metin);
		}
		
		catch(final Exception exception){
			driver.quit();
			try{
				throw new Exception(exception.getMessage());
			}
			
			catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	
	
}
