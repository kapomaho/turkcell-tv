package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import base.MobileFramework;

public class ChannelListPage extends MobileFramework {
	
	public AndroidDriver driver;
	public WebElement element;
	ExtentTest test;
	
	//android:id/ok
	

	
	@AndroidFindBy(id="com.turkcell.ott:id/search_box_input")
	public AndroidElement searchChannel;
	
	@AndroidFindBy(id="com.turkcell.ott:id/player_button_record")
	public AndroidElement kaydet;
	
	@AndroidFindBy(xpath="//*[@content-desc='Kay�t']")
	public AndroidElement kaydet2;
	
	//@AndroidFindBy(xpath="//*[@content-desc='Kay�t']")
	@AndroidFindBy(id="com.turkcell.ott:id/player_button_record")
	public List<AndroidElement> gotIt;
	
	@AndroidFindBy(xpath="//*[@content-desc='Kay�t']")
	public List<AndroidElement> kayd;
	
	@AndroidFindBy(id="com.turkcell.ott:id/record_now_button")
	public AndroidElement programiKaydet;
	
	@AndroidFindBy(id="com.turkcell.ott:id/record_minute")
	public AndroidElement record_minute;
	//com.turkcell.ott:id/record_minute
		
	
	@AndroidFindBy(id="android:id/minutes")
	public AndroidElement id_minutes;
	
	@AndroidFindBy(id="com.turkcell.ott:id/search_box_input_icon_dummy")
	public AndroidElement araButton;
	
	@AndroidFindBy(id="com.turkcell.ott:id/record_minute")
	public AndroidElement minutes;
	
	@AndroidFindBy(xpath="//*[@content-desc='5']")
	public AndroidElement five;
	
	//TAMAM
	@AndroidFindAll({
		@AndroidBy(xpath="//*[@text='TAMAM']"),
	@AndroidBy(xpath="//*[@text='Tamam']"),
	})
	public AndroidElement tamamButton;
	
	@AndroidFindBy(id="com.turkcell.ott:id/positiveButton")
	public AndroidElement recordOk;
	
	//@AndroidFindAll({
		//@AndroidBy(id="com.turkcell.ott:id/player_btn_close_top_left")	,
	@AndroidFindBy(xpath="//android.widget.ImageView[@content-desc='Geri']")	
	//})
	public List<AndroidElement> geri;
	
	//com.turkcell.ott:id/record_minute	
	@AndroidFindBy(xpath="//*[@text='KANAL D HD']")
	public AndroidElement kanalD;
	//KANAL D HD
	public ChannelListPage(AndroidDriver driver,ExtentTest test)	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait =new WebDriverWait(driver,30);
		this.test=test;
	}	

	
	public MainPage kanalAra() throws InterruptedException
	{
		TouchAction action = new TouchAction(driver);		

		/*
		System.out.println(driver.currentActivity());

		Thread.sleep(10000);

		TouchAction action = new TouchAction(driver);		
		action.longPress(10,200);

		action.release();		
		action.perform();
		System.out.println(driver.currentActivity());
*/
		click(araButton);
		test.log(LogStatus.PASS,"Click on Search button");
		write(searchChannel,"Kanal D");
		test.log(LogStatus.PASS,"Succesfully wrote channel name");
		click(kanalD);
		test.log(LogStatus.PASS,"Click on the channel");

		
		Thread.sleep(10000);
		
		action.longPress(10,250); //200 du
		action.release();		
		action.perform();
		
		//click(kaydet2);
		
		if(kayd.size()>0)
		{
			System.out.println(kayd.size());
			//for(int i=0;i<kayd.size();i++)
			click(kayd.get(0));
			test.log(LogStatus.PASS,"Click on the record button");
			//if(gotIt.size()>0)
			//click(gotIt.get(0));
		}
		
		
		
		click(minutes);
		test.log(LogStatus.PASS,"Click on the minutes");
		click(id_minutes);
		test.log(LogStatus.PASS,"Click on the id minutes");
		click(five);
		test.log(LogStatus.PASS,"Select the minutes");
		click(tamamButton);
		test.log(LogStatus.PASS,"Click on the ok button");
		click(recordOk);
		test.log(LogStatus.PASS,"Click on the record");
		Thread.sleep(3000);
		action.longPress(10,250);  //200du
 
		action.release();		
		action.perform();
		
		click(geri.get(0));
		test.log(LogStatus.PASS,"Click on the back button");

		
		return new MainPage(this.driver,this.test);

	}

	
	
}
