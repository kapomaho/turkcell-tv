package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import base.MobileFramework;

public class LoginPage extends MobileFramework {
	
	public AndroidDriver driver;
	public WebElement element;
	ExtentTest test;
	//android.widget.TextView[contains(text(),�Family�)]
	
	@AndroidFindBy(xpath="//*[@text='ATLA']")
	public AndroidElement atla;
	
	@AndroidFindBy(id="com.turkcell.ott:id/loginUser")
	public AndroidElement telnumber;
	
	@AndroidFindBy(id="com.turkcell.ott:id/loginPassw")
	public AndroidElement password;
	

	@AndroidFindBy(id="com.turkcell.ott:id/btnLogin")
	public AndroidElement giris;
	
	
	@AndroidFindBy(id="com.turkcell.ott:id/operator_continue")
	public AndroidElement devamButton;

	
	public LoginPage(AndroidDriver driver,ExtentTest test)	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait =new WebDriverWait(driver,50);
		this.test=test;
	}	


	
	public MainPage doLogin() throws InterruptedException, FileNotFoundException, IOException
	{
		System.out.println("das");
		System.out.println(driver.currentActivity());

		click(atla);
		  test.log(LogStatus.PASS,"Pressed the skip");

		write(telnumber, getConfDepend("username"));
		//beklemeSuresiBelirt(1000);
		write(password, getConfDepend("password"));
		click(giris);
		test.log(LogStatus.PASS,"Login succesful");
		click(devamButton);
		test.log(LogStatus.PASS,"Getting Home Page");
		
		return new MainPage(this.driver,this.test);

		
	}
	
	public MainPage withoutLogin() throws InterruptedException
	{
		
		click(devamButton);
		test.log(LogStatus.PASS,"Main Page opened");
		return new MainPage(this.driver,this.test);

		
	}
	
}
