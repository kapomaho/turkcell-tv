package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.time.Duration;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;

import base.MobileFramework;

public class ChannelPage extends MobileFramework {
	
	public AndroidDriver driver;
	public WebElement element;
	ExtentTest test;
	
	//android:id/ok     Kanal Listesi
	
	@AndroidFindBy(xpath="//*[@text='Kanal Listesi']")
	public AndroidElement kanalListesi;
	
	
	@AndroidFindBy(id="android:id/ok")
	public List<AndroidElement> gotIt;
	
	@AndroidFindBy(id="com.turkcell.ott:id/action_bar_root")
	public AndroidElement touch;
	
	
	public ChannelPage(AndroidDriver driver,ExtentTest test)	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait =new WebDriverWait(driver,10);
		this.test=test;
	}	

	
	public ChannelListPage gotIt() throws InterruptedException
	{
		System.out.println(driver.currentActivity());

	if(gotIt.size()>0)
		click(gotIt.get(0));
	
	//click(kanalListesi);
	
	
TouchAction action = new TouchAction(driver);
	
	
	Dimension d = driver.manage().window().getSize();
	int height = d.height;
	int width = d.width;

			
	
	action.tap(width-40,height-50);
	//action.longPress(10,200);
		action.release();		
		action.perform();
		click(kanalListesi);
	


		return new ChannelListPage(this.driver,this.test);		
	}

	
	
}
