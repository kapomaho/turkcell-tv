package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import base.MobileFramework;

public class MainPage extends MobileFramework {
	
	public AndroidDriver driver;
	public WebElement element;
	ExtentTest test;

	//gecici
	 
	@AndroidFindBy(xpath="//*[@content-desc='Kay�t']")
	public AndroidElement kaydet2;
	
	//@AndroidFindBy(xpath="//*[@content-desc='Kay�t']")
	@AndroidFindBy(id="com.turkcell.ott:id/player_button_record")
	public List<AndroidElement> gotIt;
	
	///////////////////////////////////////
	
	@AndroidFindBy(id="com.turkcell.ott:id/channel_poster")
	public List<AndroidElement> channelPoster;
	
	
	@AndroidFindBy(xpath="//*[@text='Bana �zel']")
	public AndroidElement banaOzel;
	
	@AndroidFindBy(xpath="//*[@text='KANAL D HD']")
	public AndroidElement kanald;
	
	@AndroidFindBy(id="com.turkcell.ott:id/watch_now")
	public AndroidElement hemenTvIzle;
	
	@AndroidFindBy(id="com.turkcell.ott:id/top_button")
	public AndroidElement ucCizgi;
	
	@AndroidFindBy(id="com.turkcell.ott:id/npvr_watch")
	public AndroidElement izle;
	
	public MainPage(AndroidDriver driver,ExtentTest test)	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait =new WebDriverWait(driver,50);
		this.test=test;
	}	

	
	public ChannelPage navigateTvIzle() throws InterruptedException
	{
		System.out.println(driver.currentActivity());

		click(hemenTvIzle);
		return new ChannelPage(this.driver,this.test);

	}

	public void banaOzelegit() throws InterruptedException
	{
		
		click(ucCizgi);
		test.log(LogStatus.PASS,"Open the hamburger menu");
		click(banaOzel);
		test.log(LogStatus.PASS,"Click on Banaozel");
		click(kanald);
		test.log(LogStatus.PASS,"Click on the Channel");
		click(izle);
		test.log(LogStatus.PASS,"Click on Watch Now");

	}
	
	public void directKanalD() throws InterruptedException
	{
		Thread.sleep(10000);
		click(channelPoster.get(0));
		Thread.sleep(5000);
		TouchAction action = new TouchAction(driver);		

		action.longPress(10,250); //200 du
		action.release();		
		action.perform();
		
		click(kaydet2);
	}
	
	
	
}
