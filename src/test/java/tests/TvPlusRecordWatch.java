package tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import base.MobileFramework;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import pages.LoginPage;

public class TvPlusRecordWatch extends MobileFramework {
	
	static ExtentReports report;
	static ExtentTest test;
	
	
	@BeforeTest
	  public void beforeTest() {
		 
		  report = new ExtentReports("C:\\Users\\Mahir\\Documents\\TurkcellReport.html",true);
		  test = report.startTest("TvPlusRecordChannel");
		  test.log(LogStatus.INFO,"Test Started");

	 }
	
  @Test
  public void testTVPlusRecord() throws FileNotFoundException, IOException, InterruptedException {
	  
init("Test 1");	  
//IntroPage enters=new IntroPage((AppiumDriver<MobileElement>) driver);

LoginPage enters=new LoginPage( driver,test);

//enters.doLogin();
//enters.doLogin().navigateTvIzle().gotIt().kanalAra().banaOzelegit();
enters.doLogin().navigateTvIzle().gotIt().kanalAra().banaOzelegit();



  }
  
  
	@AfterMethod
	  public void afterTest(ITestResult result) {
		 /*
		File source = null;

		  if(result.getStatus() == ITestResult.FAILURE)
		  {
			  try 
				{
				TakesScreenshot ts=(TakesScreenshot)driver;
				 
				 source=ts.getScreenshotAs(OutputType.FILE);
				 
				FileUtils.copyFile(source, new File("./Screenshots/"+"kapo"+".png"));
				 
				System.out.println("Screenshot taken");
				} 
				catch (Exception e)
				{
				 
				System.out.println("Exception while taking screenshot "+e.getMessage());
				} 
			String screenshot_path=source.getAbsolutePath();
			System.out.println(screenshot_path);
			String image= test.addScreenCapture(screenshot_path);
			test.log(LogStatus.FAIL, "Title verification", image);

*/
			
		  report.endTest(test);
		  report.flush();
		  }
		  
		 
		  
}

